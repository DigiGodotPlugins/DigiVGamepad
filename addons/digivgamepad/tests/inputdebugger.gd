extends Panel

# Public variables
onready var debug_toggle = get_node(@"Container/DebugToggle")
onready var debug_type_combo = get_node(@"Container/DebugTypeCombo")
onready var clear_button = get_node(@"Container/ClearButton")
onready var debugger = get_node(@"Debugger")

func _ready():	
	debugger.set_scroll_follow(true)
	debug_type_combo.add_item("All", 0)
	debug_type_combo.add_item("Key", 1)
	debug_type_combo.add_item("Mouse Motion", 2)
	debug_type_combo.add_item("Mouse Button", 3)
	debug_type_combo.add_item("Joystick Motion", 4)
	debug_type_combo.add_item("Joystick Button", 5)
	debug_type_combo.add_item("Screen Touch", 6)
	debug_type_combo.add_item("Screen Drag", 7)
	debug_type_combo.add_item("Action", 8)
	
	debug_type_combo.select(0)
	
	clear_button.connect("pressed", self, "_on_clear_pressed")
	
	set_process_input(true)
	set_process_unhandled_input(true)

func _input_event(event):
	if debug_type_combo.get_selected_ID() == 0 and debug_toggle.is_pressed() == true:
		debugger.add_text("%s\n" % event)
		return
		
	if event.type == debug_type_combo.get_selected_ID() and debug_toggle.is_pressed() == true:
		debugger.add_text("%s\n" % event)
		debugger.set_enab

func _unhandled_input(event):
	if debug_type_combo.get_selected_ID() == 0 and debug_toggle.is_pressed() == true:
		debugger.add_text("%s\n" % event)
		return
		
	if event.type == debug_type_combo.get_selected_ID() and debug_toggle.is_pressed() == true:
		debugger.add_text("%s\n" % event)

func _on_clear_pressed():
	debugger.clear()