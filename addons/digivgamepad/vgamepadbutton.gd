extends BaseButton

# Public variables
var linked_actions = []

# Exported variables
export(Color) var color = Color(1.0,1.0,1.0)
export(int) var button_index = 0 setget _set_button_index
export(int) var device_index = 0 setget _set_device_index

func _ready():
	update_action_mappings()
	connect("button_down", self, "_on_press")
	connect("button_up", self, "_on_release")

func _draw():
	if is_pressed() == true:
		draw_circle(Vector2(get_size().x / 2, get_size().y / 2),(min(get_size().y, get_size().x) / 2) - 10, Color(color.r,color.g,color.b,0.4))
	else:
		draw_circle(Vector2(get_size().x / 2, get_size().y / 2),(min(get_size().y, get_size().x) / 2) - 10, Color(color.r,color.g,color.b,0.2))

func _on_press():
	var event = InputEvent()
	event.type = InputEvent.JOYSTICK_BUTTON
	event.button_index = button_index
	event.device = device_index
	event.pressed = true
	event.pressure = 0.0
	
	get_tree().input_event(event)
	
	send_actions(true)
	
func _on_release():
	var event = InputEvent()
	event.type = InputEvent.JOYSTICK_BUTTON
	event.button_index = button_index
	event.device = device_index
	event.pressed = false
	event.pressure = 0.0
	
	get_tree().input_event(event)
	
	send_actions(false)

func _set_button_index(index):
	button_index = index
	update_action_mappings()

func _set_device_index(index):
	device_index = index
	update_action_mappings()
		
func send_actions(pressed):
	for action in linked_actions:
		if pressed == true:
			Input.action_press(action)
		else:
			Input.action_release(action)

func update_action_mappings():
	var event = InputEvent()
	event.type = InputEvent.JOYSTICK_BUTTON
	event.button_index = button_index
	event.device = device_index
	event.pressed = true
	event.pressure = 0.0

	linked_actions.clear()
	
	for action in InputMap.get_actions():
		if InputMap.action_has_event(action, event) == true:
			linked_actions.append(action)