extends Container

# Public variables
var vgamepad = preload("res://addons/digivgamepad/vgamepad.tscn")

# Exported variables
export(Color) var stick_background_color = Color(1.0,1.0,1.0) setget _set_stick_background_color
export(Color) var stick_color = Color(1.0,1.0,1.0) setget _set_stick_color
export(Color) var button1_color = Color(1.0,1.0,1.0) setget _set_button1_color
export(Color) var button2_color = Color(1.0,1.0,1.0) setget _set_button2_color
export(Color) var button3_color = Color(1.0,1.0,1.0) setget _set_button3_color
export(Color) var button4_color = Color(1.0,1.0,1.0) setget _set_button4_color
export(Color) var button5_color = Color(1.0,1.0,1.0) setget _set_button5_color
export(Color) var button6_color = Color(1.0,1.0,1.0) setget _set_button6_color
export(Color) var button7_color = Color(1.0,1.0,1.0) setget _set_button7_color
export(Color) var button8_color = Color(1.0,1.0,1.0) setget _set_button8_color
export(Color) var button9_color = Color(1.0,1.0,1.0) setget _set_button9_color
export(float) var stick1_action_hysteresis_value = 0.6 setget _set_stick1_action_hysteresis_value
export(float) var stick2_action_hysteresis_value = 0.6 setget _set_stick2_action_hysteresis_value

export(int) var device_index = 0 setget _set_device_index
export(int,0,9) var number_of_buttons = 3 setget _set_number_of_buttons
export(int,0,2) var number_of_sticks = 2 setget _set_number_of_sticks

func _ready():
	add_child(vgamepad.instance())
	
	self.stick_background_color = stick_background_color
	self.stick_color = stick_color
	self.button1_color = button1_color
	self.button2_color = button2_color
	self.button3_color = button3_color
	self.button4_color = button4_color
	self.button5_color = button5_color
	self.button6_color = button6_color
	self.button7_color = button7_color
	self.button8_color = button8_color
	self.button9_color = button9_color
	self.stick1_action_hysteresis_value = stick1_action_hysteresis_value
	self.stick2_action_hysteresis_value = stick2_action_hysteresis_value
	self.device_index = device_index
	self.number_of_buttons = number_of_buttons
	self.number_of_sticks = number_of_sticks

func _set_stick_background_color(gamepad_stick_background_color):
	stick_background_color = gamepad_stick_background_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/Stick1").background_color = stick_background_color
	get_node(@"VGamepad/Stick2").background_color = stick_background_color

func _set_stick_color(gamepad_stick_color):
	stick_color = gamepad_stick_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/Stick1").joystick_color = stick_color
	get_node(@"VGamepad/Stick2").joystick_color = stick_color

func _set_button1_color(gamepad_button1_color):
	button1_color = gamepad_button1_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button1").color = button1_color

func _set_button2_color(gamepad_button2_color):
	button2_color = gamepad_button2_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button2").color = button2_color

func _set_button3_color(gamepad_button3_color):
	button3_color = gamepad_button3_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button3").color = button3_color

func _set_button4_color(gamepad_button4_color):
	button1_color = gamepad_button4_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button4").color = button4_color

func _set_button5_color(gamepad_button5_color):
	button5_color = gamepad_button5_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button5").color = button5_color

func _set_button6_color(gamepad_button6_color):
	button6_color = gamepad_button6_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button6").color = button6_color

func _set_button7_color(gamepad_button7_color):
	button7_color = gamepad_button7_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button7").color = button7_color

func _set_button8_color(gamepad_button8_color):
	button8_color = gamepad_button8_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button8").color = button8_color

func _set_button9_color(gamepad_button9_color):
	button9_color = gamepad_button9_color
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/ButtonContainer/Button9").color = button9_color

func _set_stick1_action_hysteresis_value(gamepad_stick1_action_hysteresis_value):
	stick1_action_hysteresis_value = gamepad_stick1_action_hysteresis_value
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/Stick1").action_hysteresis_value = stick1_action_hysteresis_value

func _set_stick2_action_hysteresis_value(gamepad_stick2_action_hysteresis_value):
	stick2_action_hysteresis_value = gamepad_stick2_action_hysteresis_value
	
	if is_inside_tree() == false:
		return
	
	get_node(@"VGamepad/Stick2").action_hysteresis_value = stick2_action_hysteresis_value

func _set_device_index(gamepad_device_index):
	device_index = gamepad_device_index
	
	if is_inside_tree() == false:
		return

	get_node(@"VGamepad/Stick1").device_index = device_index
	get_node(@"VGamepad/Stick2").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button1").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button2").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button3").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button4").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button5").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button6").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button7").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button8").device_index = device_index
	get_node(@"VGamepad/ButtonContainer/Button9").device_index = device_index

func _set_number_of_buttons(gamepad_number_of_buttons):
	number_of_buttons = gamepad_number_of_buttons
	
	if is_inside_tree() == false:
		return

	var index = 1
	
	for button in get_node(@"VGamepad/ButtonContainer").get_children():
		if index <= number_of_buttons:
			button.set_hidden(false)
		else:
			button.set_hidden(true)
		
		index += 1

func _set_number_of_sticks(gamepad_number_of_sticks):
	number_of_sticks = gamepad_number_of_sticks
	
	if is_inside_tree() == false:
		return
	
	if number_of_sticks == 2:
		get_node(@"VGamepad/Stick1").set_hidden(false)
		get_node(@"VGamepad/Stick2").set_hidden(false)
	elif number_of_sticks == 1:
		get_node(@"VGamepad/Stick1").set_hidden(false)
		get_node(@"VGamepad/Stick2").set_hidden(true)
	else:
		get_node(@"VGamepad/Stick1").set_hidden(true)
		get_node(@"VGamepad/Stick2").set_hidden(true)

func update_action_mappings():
	get_node(@"VGamepad/Stick1").update_action_mappings()
	get_node(@"VGamepad/Stick2").update_action_mappings()
	get_node(@"VGamepad/ButtonContainer/Button1").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button2").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button3").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button4").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button5").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button6").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button7").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button8").update_actions_mappings()
	get_node(@"VGamepad/ButtonContainer/Button9").update_actions_mappings()
	