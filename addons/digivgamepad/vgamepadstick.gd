extends Control

# Public variables
var touch_pressed = false
var touch_index = -1
var stick_value = Vector2(0.0,0.0)
var send_stick_value = false
var positive_axis1_linked_actions = []
var negative_axis1_linked_actions = []
var positive_axis2_linked_actions = []
var negative_axis2_linked_actions = []
var last_action_values = [false,false,false,false]
var last_axis_values = [0.0,0.0]

const POSITIVE_AXIS1 = 0
const NEGATIVE_AXIS1 = 1
const POSITIVE_AXIS2 = 2
const NEGATIVE_AXIS2 = 3

# Exported variables
export(Color) var background_color  = Color(1.0,1.0,1.0)
export(Color) var joystick_color  = Color(1.0,1.0,1.0)
export(int) var axis1_index = 0 setget _set_axis1_index
export(int) var axis2_index = 0 setget _set_axis2_index
export(int) var device_index = 0 setget _set_device_index
export(float) var action_hysteresis_value = 0.6

func _ready():
	_send_axis_events()
	set_process_input(true)
	set_process(true)

func _draw():
	if is_inside_tree() == false and is_visible() == false:
		return

	draw_circle(Vector2(get_size().x / 2, get_size().y / 2),(min(get_size().y, get_size().x) / 2) - 10, Color(background_color.r,background_color.g,background_color.b,0.2))
	draw_circle(Vector2((stick_value.x * (min(get_size().y, get_size().x) / 2.0)) + (get_size().x / 2.0), (stick_value.y * (min(get_size().y, get_size().x) / 2.0)) + (get_size().y / 2.0)),min(get_size().y, get_size().x) / 8, Color(joystick_color.r,joystick_color.g,joystick_color.b,0.5))

func _input(event):
	if is_inside_tree() == false and is_visible() == false:
		return
		
	var center = get_global_pos() + (get_size() / 2.0)
	var joystick_distance = min(get_size().y, get_size().x) / 8
	var joystick_position = Vector2(0.0,0.0)

	if event.type == InputEvent.SCREEN_TOUCH and event.pressed == false and event.index == touch_index:
		touch_pressed = false
		touch_index = -1
		stick_value = Vector2(0.0,0.0)
		send_stick_value = true

		update()

	if event.type == InputEvent.SCREEN_TOUCH and event.pressed == true:
		if center.distance_to(event.pos) < joystick_distance:
			touch_pressed = event.pressed
			touch_index = event.index
	
	if event.type == InputEvent.SCREEN_DRAG and touch_pressed == true and event.index == touch_index:
		joystick_position.x = -(center.x - event.pos.x)
		joystick_position.y = (event.pos.y - center.y)
		
		joystick_position = joystick_position.clamped(min(get_size().y, get_size().x) / 2.0)
		
		# Clamp around -1.0 to 1.0
		joystick_position /= min(get_size().y, get_size().x) / 2.0	
		stick_value = joystick_position
		send_stick_value = true
		
		update()

func _process(delta):
	# HACKME: We have used process() method to send axis input values because calling
	# the method directly fails obtaining touch unpressed events, and if we call as
	# deferred using call_deferred() it gets "Stack Overflow".
	if send_stick_value == true:
		_send_axis_events()
		send_stick_value = false

func _send_axis_events():
	if last_axis_values[0] != stick_value.x:
		var event = InputEvent()
		
		event.type = InputEvent.JOYSTICK_MOTION
		event.axis = axis1_index
		event.device = device_index
		event.value = stick_value.x
		
		get_tree().input_event(event)
		
		if stick_value.x > action_hysteresis_value:			
			send_actions(POSITIVE_AXIS1, true)
			send_actions(NEGATIVE_AXIS1, false)
		
		if stick_value.x < -action_hysteresis_value:			
			send_actions(NEGATIVE_AXIS1, true)
			send_actions(POSITIVE_AXIS1, false)
	
		if stick_value.x <= action_hysteresis_value and stick_value.x >= -action_hysteresis_value:			
			send_actions(POSITIVE_AXIS1, false)
			send_actions(NEGATIVE_AXIS1, false)
		
		last_axis_values[0] = stick_value.x
	
	if last_axis_values[1] != stick_value.y:
		var event = InputEvent()
		
		event.type = InputEvent.JOYSTICK_MOTION
		event.axis = axis2_index
		event.device = device_index
		event.value = stick_value.y
		
		get_tree().input_event(event)
		
		if stick_value.y > action_hysteresis_value:			
			send_actions(POSITIVE_AXIS2, true)
			send_actions(NEGATIVE_AXIS2, false)
		
		if stick_value.y < -action_hysteresis_value:			
			send_actions(NEGATIVE_AXIS2, true)
			send_actions(POSITIVE_AXIS2, false)
	
		if stick_value.y <= action_hysteresis_value and stick_value.y >= -action_hysteresis_value:
			send_actions(POSITIVE_AXIS2, false)
			send_actions(NEGATIVE_AXIS2, false)
		
		last_axis_values[1] = stick_value.y

func _set_axis1_index(index):
	axis1_index = index
	
	update_action_mappings()

func _set_axis2_index(index):
	axis2_index = index
	
	update_action_mappings()

func _set_device_index(index):
	device_index = index
	
	update_action_mappings()

func send_actions(axis, pressed):
	if last_action_values[axis] == pressed:
		return

	var actions = []
	
	if axis == POSITIVE_AXIS1:
		actions = positive_axis1_linked_actions
	elif axis == NEGATIVE_AXIS1:
		actions = negative_axis1_linked_actions
	elif axis == POSITIVE_AXIS2:
		actions = positive_axis2_linked_actions
	elif axis == NEGATIVE_AXIS2:
		actions = negative_axis2_linked_actions
	
	for action in actions:
		if pressed == true:
			Input.action_press(action)
		else:
			Input.action_release(action)
	
	last_action_values[axis] = pressed

func update_action_mappings():
	var event = InputEvent()
	event.type = InputEvent.JOYSTICK_MOTION
	event.axis = axis1_index
	event.device = device_index
	event.value = 1.0
	
	positive_axis1_linked_actions.clear()
	negative_axis1_linked_actions.clear()
	positive_axis2_linked_actions.clear()
	negative_axis2_linked_actions.clear()
	
	for action in InputMap.get_actions():
		if InputMap.action_has_event(action, event) == true:
			positive_axis1_linked_actions.append(action)
	
	event.value = -1.0
	
	for action in InputMap.get_actions():
		if InputMap.action_has_event(action, event) == true:
			negative_axis1_linked_actions.append(action)
	
	event.axis = axis2_index
	event.value = 1.0
	
	for action in InputMap.get_actions():
		if InputMap.action_has_event(action, event) == true:
			positive_axis2_linked_actions.append(action)
	
	event.value = -1.0
	
	for action in InputMap.get_actions():
		if InputMap.action_has_event(action, event) == true:
			negative_axis2_linked_actions.append(action)
