tool
extends EditorPlugin

func _enter_tree():
	add_custom_type("VGamepad", "Container", preload("vgamepad.gd"), preload("vgamepad.png"))	
	add_custom_type("VGamepadStick", "Control", preload("vgamepadstick.gd"), preload("vgamepad.png"))	
	add_custom_type("VGamepadButton", "BaseButton", preload("vgamepadbutton.gd"), preload("vgamepad.png"))	

func _exit_tree():
	remove_custom_type("VGamepad")
	remove_custom_type("VGamepadStick")
	remove_custom_type("VGamepadButton")